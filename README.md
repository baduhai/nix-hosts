My nix configs.

Current hosts:

| Host | NixOS version | Notes |
|:--- |:---:| ---:|
| rotterdam | unstable | Main workstation |
| explorer | unstable | Work laptop |
| rothenburg | 22.05 | RasPi3 server |
| alexandria | 22.05 | NAS with various utilities |
| rome | 22.05 | Home router. Not in production, slowly working on it |

For dotfiles, see [baduhai/dotfiles](https://github.com/baduhai/dotfiles). Managed with nix and home-manager.
